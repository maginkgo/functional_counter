import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:functional_widget_annotation/functional_widget_annotation.dart';

part 'app.g.dart';

@widget
Widget app() {
  return ChangeNotifierProvider(
    builder: (_) => Counter(),
    child: MaterialApp(home: Home()),
  );
}

@widget
Widget home(BuildContext context) {
  var counter = Provider.of<Counter>(context);
  return Scaffold(
    body: Body("${counter.value}"),
    floatingActionButton: Fab(counter.setValue),
  );
}

@widget
Widget body(String value) => Center(child: Text(value));

@widget
Widget fab(VoidCallback onPressed) {
  return FloatingActionButton(
    child: Icon(Icons.add),
    onPressed: onPressed,
  );
}

class Counter extends ChangeNotifier {
  int _value = 0;

  int get value => _value;

  void setValue() {
    _value++;
    notifyListeners();
  }
}
