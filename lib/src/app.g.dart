// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'app.dart';

// **************************************************************************
// FunctionalWidgetGenerator
// **************************************************************************

class App extends StatelessWidget {
  const App({Key key}) : super(key: key);

  @override
  Widget build(BuildContext _context) => app();
}

class Home extends StatelessWidget {
  const Home({Key key}) : super(key: key);

  @override
  Widget build(BuildContext _context) => home(_context);
}

class Body extends StatelessWidget {
  const Body(this.value, {Key key}) : super(key: key);

  final String value;

  @override
  Widget build(BuildContext _context) => body(value);
}

class Fab extends StatelessWidget {
  const Fab(this.onPressed, {Key key}) : super(key: key);

  final VoidCallback onPressed;

  @override
  Widget build(BuildContext _context) => fab(onPressed);
}
